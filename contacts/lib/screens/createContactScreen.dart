import 'dart:io';

import 'package:contacts/helpers/contactHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CreatContactScreen extends StatefulWidget {

  final Contact contact;

  CreatContactScreen({this.contact});

  @override
  _CreatContactScreenState createState() => _CreatContactScreenState();
}

class _CreatContactScreenState extends State<CreatContactScreen> {

  final TextEditingController _nameController = TextEditingController();
  final _nameFocus = FocusNode();
  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _phoneController = TextEditingController();
  final ContactHelper _helper = ContactHelper();

  Contact _contact;
  bool _isEdited = false;

  @override
  void initState() {
    super.initState();

    if (widget.contact == null) {
      _contact = Contact();
    } else {
      _contact = Contact.fromMap(widget.contact.toMap()); //deep copy

      setState(() {
        _nameController.text = _contact.name;
        _emailController.text = _contact.email;
        _phoneController.text = _contact.phone;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: Text(_contact.name ?? 'Novo contato'),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (_contact.name != null && _contact.name.isNotEmpty) {
              Navigator.pop(context, _contact);
            } else {
              FocusScope.of(context).requestFocus(_nameFocus);
            }
          },
          child: Icon(Icons.save),
          backgroundColor: Colors.red,
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              GestureDetector(
                child: Container(
                  width: 140.0,
                  height: 140.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: _contact.img == null ? AssetImage('images/person.png') : FileImage(File(_contact.img)),
                    ),
                  ),
                ),
                onTap: () {
                  ImagePicker.pickImage(source: ImageSource.camera).then((file) {
                    if (file == null) {
                      return;
                    }

                    setState(() {
                      _contact.img = file.path;
                    });
                  });
                },
              ),
              TextField(
                controller: _nameController,
                focusNode: _nameFocus,
                decoration: InputDecoration(labelText: 'Nome'),
                onChanged: (text) {
                  _isEdited = true;
                  setState(() {
                    _contact.name = text;
                  });
                },
              ),
              TextField(
                controller: _emailController,
                decoration: InputDecoration(labelText: 'E-mail'),
                onChanged: (text) {
                  _isEdited = true;
                  setState(() {
                    _contact.email = text;
                  });
                },
                keyboardType: TextInputType.emailAddress,
              ),
              TextField(
                controller: _phoneController,
                decoration: InputDecoration(labelText: 'Fone'),
                onChanged: (text) {
                  _isEdited = true;
                  setState(() {
                    _contact.phone = text;
                  });
                },
                keyboardType: TextInputType.phone,
              )
            ],
          ),
        ),
      )
    );
  }

  Future<bool> _requestPop() {
    if (_isEdited) {
      showDialog(context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Descartar alterações'),
            content: Text('Se sair as alterações serão perdidas.'),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text('Confirmar'),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
              )
            ],
          );
        }
      );
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }
}
