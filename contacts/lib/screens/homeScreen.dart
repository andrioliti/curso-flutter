import 'dart:io';

import 'package:contacts/helpers/contactHelper.dart';
import 'package:contacts/screens/createContactScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

enum OrderOptions {orderaz, orderza}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  ContactHelper _contactHelper = ContactHelper();
  List<Contact> _contacts = List();

  @override
  void initState() {
    super.initState();

    _getAllContact();
  }

  void _getAllContact() {
     _contactHelper.getAll().then((contacts) {
      setState(() {
        _contacts = contacts;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemCount: _contacts.length,
        itemBuilder: (context, index) {
          return _buildCard(_contacts[index]);
        }),
      appBar: AppBar(
        title: Text('Contatos'),
        backgroundColor: Colors.red,
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<OrderOptions>(
            itemBuilder: (context) => <PopupMenuEntry<OrderOptions>>[
              const PopupMenuItem<OrderOptions>(child: Text('Ordenar de A-Z')),
              const PopupMenuItem<OrderOptions>(child: Text('Ordenar de Z-A')),
            ],
            onSelected: (OrderOptions order) {
              switch (order) {
                case OrderOptions.orderaz:
                  setState(() {
                    _contacts.sort((a,b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));
                  });
                break;
                case OrderOptions.orderza:
                  setState(() {
                    _contacts.sort((a,b) => b.name.toLowerCase().compareTo(a.name.toLowerCase()));
                  });
                break;
              }
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showContactScreen();
        },
        child: Icon(
          Icons.add,
        ),
        backgroundColor: Colors.red,
      ),
    );
  }

  Widget _buildCard(Contact contact) {
    return GestureDetector(
      onTap: () {
        _showContactOptions(context, contact);
        //_showContactScreen(contact: contact);
      },
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: contact.img == null || contact.img.isEmpty ? AssetImage('images/person.png') : FileImage(File(contact.img)),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10.0,),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      contact.name ?? '',
                      style: TextStyle(
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      contact.email ?? '',
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    Text(
                      contact.phone ?? '',
                      style: TextStyle(
                        fontSize: 22.0,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showContactScreen({Contact contact}) async {
    final resContact = await Navigator.push(context, MaterialPageRoute(builder: (context) => CreatContactScreen(contact: contact)));

    if (resContact != null) {
      if (contact != null) {
        await _contactHelper.updateContact(resContact);
      } else {
        await _contactHelper.saveContact(resContact);
      }

      _getAllContact();
    }
  }

  void _showContactOptions(BuildContext context, Contact contact) {
    showModalBottomSheet(context: context, builder: (context) {
      return BottomSheet(
        onClosing: () { },
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: FlatButton(
                    child: Text('Ligar', style: TextStyle(color: Colors.red, fontSize: 20.0),),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: FlatButton(
                    child: Text('Editar', style: TextStyle(color: Colors.red, fontSize: 20.0),),
                    onPressed: () {
                      Navigator.pop(context);
                      _showContactScreen(contact: contact);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: FlatButton(
                    child: Text('Excluir', style: TextStyle(color: Colors.red, fontSize: 20.0),),
                    onPressed: () {
                      _contactHelper.deleteContact(contact.id);
                      _getAllContact();
                      Navigator.pop(context);
                    },
                  ),
                ),
              ],
            ),
          );
        },
      );
    });
  }
}
