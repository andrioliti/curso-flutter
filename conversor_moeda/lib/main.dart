import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

const API_URL = "https://api.hgbrasil.com/finance?key=f9d011d5";

void main() async {
  print(await getData());

  runApp(MaterialApp(
    theme: ThemeData(
        inputDecorationTheme: InputDecorationTheme(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white)))),
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.yellowAccent[400],
        title: Text("\$Conversor\$"),
        centerTitle: true,
      ),
      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              if (snapshot.hasError) {
                return Loading("Erro ao carregar os dados");
              } else {
                return Conversor(snapshot.data);
              }
            } else {
              return Loading("Carregando");
            }
          }),
    );
  }
}

class Loading extends StatelessWidget {
  String _msg;

  Loading(String msg) {
    this._msg = msg;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(this._msg,
          style: TextStyle(
            color: Colors.amber,
            fontSize: 25.0,
          ),
          textAlign: TextAlign.center),
    );
  }
}

class Conversor extends StatefulWidget {

  Map data;

  Conversor(Map data) {
    data = data;
  }

  @override
  _ConversorState createState() => _ConversorState();
}

class _ConversorState extends State<Conversor> {
  double dollar;
  double euro;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(widget.data.toString()),
    );
  }
}

Future<Map> getData() async {
  http.Response res = await http.get(API_URL);

  return json.decode(res.body);
}
