import 'package:flutter/material.dart';
import 'package:gify/components/TheAppBar.dart';
import 'package:gify/screens/homePage.dart';

void main() => runApp(GifyApp());

class GifyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: buildAppbar(),
        body: HomePage(),
      ),
      theme: ThemeData(hintColor: Colors.white),
    );
  }
}

