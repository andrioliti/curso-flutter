import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gify/screens/viewGiphy.dart';
import 'package:gify/services/gifyService.dart';
import 'package:share/share.dart';
import 'package:transparent_image/transparent_image.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController _searchController = TextEditingController();
  String _query = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
                labelText: "Pesquise Aqui!",
                labelStyle: TextStyle(color: Colors.white),
                border: OutlineInputBorder()
            ),
            onSubmitted: (query) {
              setState(() {
                _query = query;
              });
            },
            style: TextStyle(color: Colors.white, fontSize: 18.0),
            textAlign: TextAlign.center,
            controller: _searchController,
          ),
          Expanded(
            child: FutureBuilder(
              future: getGifs(_query.isEmpty ? null : { 'query': _query, 'offset': 0}),
              builder: (context, snapshot) {
                switch(snapshot.connectionState) {
                  case ConnectionState.waiting:
                  case ConnectionState.none:
                    return Container(
                      width: 200.0,
                      height: 200.0,
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                        strokeWidth: 5.0,
                      ),
                    );
                  default:
                    return _createGiffTable(context, snapshot);
                }
              },
            ),
          )
        ],
      ),
    );
  }
}

Widget _createGiffTable(context, snapshot) {
  return GridView.builder(
      padding: EdgeInsets.all(10.0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
      ),
      itemCount: snapshot.data['data'].length,
      itemBuilder: (context, index) {
        return GestureDetector(
          child: FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: snapshot.data['data'][index]['images']['fixed_height']['url'],
              fit: BoxFit.fill
          ),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => ViewGiphy(snapshot.data['data'][index])));
          },
          onLongPress: () {
            Share.share(snapshot.data['data'][index]['images']['fixed_height']['url']);
          },
        );
      }
  );
}