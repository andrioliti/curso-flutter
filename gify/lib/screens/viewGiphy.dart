import 'package:flutter/material.dart';
import 'package:share/share.dart';

class ViewGiphy extends StatelessWidget {

  final Map _giphyData;

  ViewGiphy(this._giphyData);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(_giphyData['title']),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Share.share(_giphyData['images']['fixed_height']['url']);
            },
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Image.network(_giphyData['images']['fixed_height']['url']),
      ),
    );
  }
}

