import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<Map> getGifs(search) async {
  http.Response res;

  if (search == null) {
    res = await http.get('https://api.giphy.com/v1/gifs/trending?api_key=iv5YCOdMcz5Biry5TRKAEwE8WMuIYhtF&limit=20&rating=G');
  } else {
    res = await http.get('https://api.giphy.com/v1/gifs/search?api_key=iv5YCOdMcz5Biry5TRKAEwE8WMuIYhtF&q=${search["query"]}&limit=20&offset=${search["offset"]}&rating=G&lang=en');
  }

  return json.decode(res.body);
}