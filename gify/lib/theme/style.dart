import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
    hintColor: Colors.white,
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
        color: Colors.white,
        fontSize: 18.0,
      ),
      border: OutlineInputBorder()
    ),
  );
}