import 'package:flutter/material.dart';
import 'package:loja/screens/cart.screen.dart';

class CartButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (_) => CartScreen()),
        );
      },
      child: IconButton(
        icon: Icon(Icons.shopping_cart, color: Colors.white,),
      ),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }
}
