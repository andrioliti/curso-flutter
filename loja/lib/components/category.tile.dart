import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja/screens/category.screen.dart';

class CategoryTile extends StatelessWidget {

  final DocumentSnapshot snp;

  CategoryTile(this.snp);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        radius: 25,
        backgroundColor: Colors.transparent,
        backgroundImage: NetworkImage(snp.data['icon'])
      ),
      title: Text(snp.data['title']),
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => CategoryScreen(snp)));
      },
    );
  }
}
