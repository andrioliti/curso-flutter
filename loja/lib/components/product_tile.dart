import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loja/models/product.model.dart';
import 'package:loja/screens/product.screen.dart';

class ProductTile extends StatelessWidget {
  final String type;
  final ProductModel model;

  ProductTile(this.type, this.model);

  Widget _buildGridTile(BuildContext context) => Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    mainAxisAlignment: MainAxisAlignment.start,
    children: <Widget>[
      AspectRatio(
        aspectRatio: 0.8,
        child: Image.network(model.images[0], fit: BoxFit.cover,),
      ),
      Expanded(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Text(model.title,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text("R\$ ${model.price.toStringAsFixed(2)}",
                style: TextStyle(
                  color: Theme.of(context).primaryColor
                )
              ),
            ],
          ),
        ),
      )
    ],
  );

  Widget _buildListTile(BuildContext context) =>  Row(
    children: <Widget>[
      Flexible(
        flex: 1,
        child: Image.network(model.images[0], fit: BoxFit.cover,),
      ),
      Flexible(
        flex: 1,
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(model.title,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text("R\$ ${model.price.toStringAsFixed(2)}",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor
                  )
              ),
            ],
          ),
        ),
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        child: type == 'grid' ? _buildGridTile(context) : _buildListTile(context),
      ),
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (ctx) =>ProductScreen(model)));
      },
    );
  }
}
