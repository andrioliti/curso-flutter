import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:loja/models/cart.product.model.dart';
import 'package:loja/models/user.model.dart';
import 'package:scoped_model/scoped_model.dart';

class CartModel extends Model {

  final UserModel userModel;

  List<CartProductModel> products = [];

  CartModel(this.userModel);

  static CartModel of(BuildContext context) => ScopedModel.of<CartModel>(context);

  addCartProduct(CartProductModel cartProductModel) {
    products.add(cartProductModel);

    Firestore.instance.collection('users').document(userModel.firebaseUser.uid)
        .collection('cart').add(cartProductModel.toMap())
        .then((doc) {
          cartProductModel.cid = doc.documentID;
        });

    notifyListeners();
  }

  removeCartProduct(CartProductModel cartProductModel) {
    Firestore.instance.collection('users').document(userModel.firebaseUser.uid)
        .collection('cart').document(cartProductModel.cid).delete();

    products.remove(cartProductModel);

    notifyListeners();
  }

}