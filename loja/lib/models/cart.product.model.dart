import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:loja/models/product.model.dart';

class CartProductModel {

  String cid;
  String category;
  String pid;

  int quantity;
  String size;

  ProductModel product;

  CartProductModel({ this.cid, this.category, this.pid, this.quantity, this.size, this.product });

  CartProductModel.fromDocument(DocumentSnapshot doc) {
    cid = doc.documentID;
    category = doc.data['category'];
    pid = doc.data['pid'];
    quantity = doc.data['quantity'];
    size = doc.data['size'];
  }

  Map<String, dynamic> toMap() => {
    "category": category,
    "pid": pid,
    "quantity": quantity,
    "size": size,
    //"product": product.toResumedMap(),
  };
}