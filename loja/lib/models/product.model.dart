import 'package:cloud_firestore/cloud_firestore.dart';

class ProductModel {
  String id;
  String category;

  String title;
  String description;

  double price;

  List<String> images;
  List<String> sizes;

  ProductModel.fromDocument(DocumentSnapshot snp) {
    id = snp. documentID;
    title = snp.data['title'];
    description = snp.data['description'];
    price = snp.data['price'];
    images = snp.data['images'].cast<String>();
    sizes = snp.data['sizes'].cast<String>();
  }

  Map<String, dynamic> toResumedMap() => {
    "title": title,
    "description": description,
    "price": price,
  };

}