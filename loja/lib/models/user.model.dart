import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class UserModel extends Model {

  bool isLoading = false;

  Map<String, dynamic> userData = Map();
  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser firebaseUser;


  static UserModel of(BuildContext context) => ScopedModel.of<UserModel>(context);

  @override
  void addListener(VoidCallback listener) async {
    super.addListener(listener);

    await _getUserData();
  }

  void singUp({
    @required Map<String, dynamic> userDataParam,
    @required String pass,
    @required VoidCallback onSuccess,
    @required VoidCallback onError
  }) async {
    isLoading = true;
    notifyListeners();

    _auth.createUserWithEmailAndPassword(
        email: userDataParam['email'], password: pass)
        .then((user) async {
      firebaseUser = user;

      await _saveUserData(userDataParam);

      onSuccess();
      isLoading = false;
      notifyListeners();
    })
        .catchError((e) {
      onError();

      print(e);

      isLoading = false;
      notifyListeners();
    });
  }

  bool isSingIn() {
    return firebaseUser != null;
  }

  void logout() async {
    await _auth.signOut();

    userData = Map();
    firebaseUser = null;

    notifyListeners();
  }

  void singIn({
    @required String email,
    @required String pass,
    @required VoidCallback onSuccess,
    @required VoidCallback onError,
  }) async {
    isLoading = true;
    notifyListeners();

    _auth.signInWithEmailAndPassword(email: email, password: pass)
      .then((user) async {
        firebaseUser = user;

        await _getUserData();

        onSuccess();
        isLoading = false;
        notifyListeners();
      }).catchError((e) {
        onError();
        isLoading = false;
        notifyListeners();
      });

  }

  void recoverPass(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  Future<Null> _saveUserData(Map<String, dynamic> userDataParam) async {
    this.userData = userDataParam;
    await Firestore.instance.collection('users').document(firebaseUser.uid).setData(userData);
  }

  Future<Null> _getUserData() async {
    if (firebaseUser == null)
      firebaseUser = await _auth.currentUser();

    if (firebaseUser != null && userData['name'] == null) {
      DocumentSnapshot docUser = await Firestore.instance.collection('users')
          .document(firebaseUser.uid)
          .get();

      userData = docUser.data;
    }

    notifyListeners();
  }
}