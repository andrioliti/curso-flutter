import 'package:flutter/material.dart';
import 'package:loja/main.dart';
import 'package:loja/models/cart.model.dart';
import 'package:scoped_model/scoped_model.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meu Carrinho'),
        centerTitle: true,
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 8),
            alignment: Alignment.center,
            child: ScopedModelDescendant<CartModel>(
              builder: (context, child, model) {
                int p = model.products.length;

                return Text("${p ?? 0} ${p <= 1 ? "ITEM" : "ITENS" }",
                  style: TextStyle(
                    fontSize: 17
                  ),
                );
              }
            )
          )
        ],
      ),
    );
  }
}
