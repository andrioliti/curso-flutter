import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja/components/product_tile.dart';
import 'package:loja/models/product.model.dart';

class CategoryScreen extends StatelessWidget {

  final DocumentSnapshot categorySnp;

  CategoryScreen(this.categorySnp);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(categorySnp.data['title']),
          centerTitle: true,
          bottom: TabBar(
            tabs: <Widget>[
              Icon(Icons.grid_on),
              Icon(Icons.format_list_bulleted)
            ],
            indicatorColor: Colors.white,
          ),
        ),
        body: FutureBuilder<QuerySnapshot>(
          future: Firestore.instance.collection('produtos')
              .document(categorySnp.documentID).collection('itens')
              .getDocuments(),
          builder: (context, productSnp) {
            if (!productSnp.hasData) {
              return Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.white),),);
            }

            return TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 4,
                    crossAxisSpacing: 4,
                    childAspectRatio: 0.65
                  ),
                  itemBuilder: (context, index) {
                    var product = ProductModel.fromDocument(productSnp.data.documents[index]);
                    product.category = categorySnp.documentID;

                    return ProductTile('grid', product);
                  },
                  itemCount: productSnp.data.documents.length ?? 0,
                ),
                ListView.builder(
                  itemBuilder: (context, index) {
                    var product = ProductModel.fromDocument(productSnp.data.documents[index]);
                    product.category = categorySnp.documentID;

                    return ProductTile('list', product);
                  },
                  itemCount: productSnp.data.documents.length ?? 0,
                )
              ],
            );
          },
        ),
      ),
    );
  }
}