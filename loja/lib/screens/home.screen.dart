import 'package:flutter/material.dart';
import 'package:loja/components/cart.button.component.dart';
import 'package:loja/components/custom.drawer.dart';
import 'package:loja/tabs/new.tab.dart';
import 'package:loja/tabs/product.tab.dart';

class HomeScreen extends StatelessWidget {

  final _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: _pageController,
      children: <Widget>[
        Scaffold(
          body: NewsTab(),
          drawer: CustomDrawer(_pageController),
          floatingActionButton: CartButton(),
        ),
        Scaffold(
          body: ProductTab(),
          appBar: AppBar(
            title: Text('Produtos'),
            centerTitle: true,
          ),
          drawer: CustomDrawer(_pageController),
          floatingActionButton: CartButton(),
        )
      ],
    );
  }
}
