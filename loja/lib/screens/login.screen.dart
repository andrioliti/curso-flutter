import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loja/models/user.model.dart';
import 'package:loja/screens/sing_up.screen.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _emailController = TextEditingController();
  final _passController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    final primaryColor = Theme.of(context).primaryColor;

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('ENTRAR'),
        centerTitle: true,
        actions: <Widget>[
          FlatButton(
            child: Text(
              'CRIAR CONTA',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => SingUpScreen()));
            },
          )
        ],
      ),
      body: ScopedModelDescendant<UserModel>(builder: (context, child, model) {

        if (model.isLoading) {
          return Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(primaryColor),),);
        }

        return Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.all(16),
            children: <Widget>[
              TextFormField(
                controller: _emailController,
                decoration: InputDecoration(hintText: 'E-mail'),
                validator: (text) =>
                    text == null || !text.contains('@') || text.length < 6 ? 'e-mail inválido.' : null,
              ),
              SizedBox(
                height: 26,
              ),
              TextFormField(
                  controller: _passController,
                decoration: InputDecoration(hintText: 'Senha'),
                obscureText: true,
                validator: (text) =>
                    text == null || text.length < 4 ? 'senha inválida' : null,
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    child: Text(
                      'Esqueceu sua senha?',
                      style: TextStyle(fontSize: 15),
                    ),
                    onPressed: () {
                      if (_emailController.text.isEmpty) {
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('E-mail não preenchido.'),
                          backgroundColor: Colors.redAccent,
                        ));
                      } else {
                        model.recoverPass(_emailController.text);

                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text('E-mail enviado.'),
                          backgroundColor: Theme.of(context).primaryColor,
                        ));
                      }
                    },
                  )),
              SizedBox(
                height: 26,
              ),
              SizedBox(
                height: 50,
                child: RaisedButton(
                  child: Text(
                    'Entrar',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      model.singIn(
                        email: _emailController.text,
                        pass: _passController.text,
                        onSuccess: () {
                          Navigator.of(context).pop();
                        },
                        onError: () {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text('Erro ao logar.'),
                            backgroundColor: Colors.redAccent,
                          ));
                        }
                      );
                    }
                  },
                  color: Theme.of(context).primaryColor,
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}
