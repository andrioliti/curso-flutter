import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:loja/models/cart.model.dart';
import 'package:loja/models/cart.product.model.dart';
import 'package:loja/models/product.model.dart';
import 'package:loja/models/user.model.dart';
import 'package:loja/screens/login.screen.dart';

class ProductScreen extends StatefulWidget {

  final ProductModel model;

  ProductScreen(this.model);

  @override
  _ProductScreenState createState() => _ProductScreenState(model);
}

class _ProductScreenState extends State<ProductScreen> {
  final ProductModel model;

  _ProductScreenState(this.model);
  String size;

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;

    return Scaffold(
      appBar: AppBar(
        title: Text(model.title),
        centerTitle: true ,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1,
              child: Carousel(
                images: model.images.map((it) => NetworkImage(it)).toList(),
                dotSize: 4,
                dotSpacing: 15,
                dotBgColor: Colors.transparent,
                dotColor: primaryColor,
                autoplay: false,
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(model.title,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20
                    ),
                  ),
                  Text("R\$ ${model.price.toStringAsFixed(2)}",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 22
                      )
                  ),
                  SizedBox(height: 16,),
                  Text('Tamanho',
                      style: TextStyle(fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 34,
                    child: GridView(
                      padding: EdgeInsets.symmetric(vertical: 4),
                      scrollDirection: Axis.horizontal,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                        mainAxisSpacing: 8,
                        childAspectRatio: 0.5,
                      ),
                      children: model.sizes.map((it) => GestureDetector(
                        onTap: () {
                          setState(() {
                            size = it;
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            border: Border.all(
                              color: it == size ? primaryColor : Colors.grey[500],
                              width: 3
                            )
                          ),
                          child: Text(it),
                          width: 64,
                          alignment: Alignment.center,
                        ),
                      )).toList(),
                    ),
                  ),
                  SizedBox(height: 16,),
                  SizedBox(
                    height: 45,
                    child: RaisedButton(
                      onPressed: size == null ? null :
                          () {
                            if (UserModel.of(context).isSingIn()) {
                              var cartProduct = CartProductModel(
                                size: size,
                                quantity: 1,
                                pid: model.id,
                              );

                              CartModel.of(context).addCartProduct(cartProduct);
                            } else {
                              Navigator.of(context).push(MaterialPageRoute(builder: (_) => LoginScreen()));
                            }
                          },
                      child: Text(UserModel.of(context).isSingIn() ? "Adicionar ao carrinho" : "Entrar para comprar",
                        style: TextStyle(
                          color: Colors.white
                        )
                      ),
                      color: primaryColor,
                    ),
                  ),
                  SizedBox(height: 16,),
                  Text('Descrição',
                    style: TextStyle(fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(model.description ?? '',
                    style: TextStyle(fontSize: 16,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
