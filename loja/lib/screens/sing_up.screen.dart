import 'package:flutter/material.dart';
import 'package:loja/models/user.model.dart';
import 'package:scoped_model/scoped_model.dart';

class SingUpScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _addressController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CRIAR CONTA'),
        centerTitle: true,
      ),
      body: ScopedModelDescendant<UserModel>(
          builder: (context, child, model) {
            if (model.isLoading) {
              return Center(child: CircularProgressIndicator(),);
            }

            return Form(
              key: _formKey,
              child: ListView(
                padding: EdgeInsets.all(16),
                children: <Widget>[
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                        hintText: 'Nome completo'
                    ),
                    validator: (text) => text.isEmpty ? 'nome inválido.' : null,
                  ),
                  SizedBox(height: 26,),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                        hintText: 'E-mail'
                    ),
                    validator: (text) =>
                    text == null || !text.contains('@') || text.length < 6
                        ? 'e-mail inválido.'
                        : null,
                  ),
                  SizedBox(height: 26,),
                  TextFormField(
                    controller: _passController,
                    decoration: InputDecoration(
                        hintText: 'Senha'
                    ),
                    obscureText: true,
                    validator: (text) =>
                    text == null || text.length < 6
                        ? 'senha inválida'
                        : null,
                  ),
                  SizedBox(height: 26,),
                  TextFormField(
                    controller: _addressController,
                    decoration: InputDecoration(
                        hintText: 'Endereço'
                    ),
                    validator: (text) =>
                    text.isEmpty
                        ? 'endereço inválido.'
                        : null,
                  ),
                  SizedBox(height: 26,),
                  SizedBox(
                    height: 50,
                    child: RaisedButton(
                      child: Text('Criar conta',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          Map<String, dynamic> userData = {
                            'name': _nameController.text,
                            'email': _emailController.text,
                            'address': _addressController.text,
                          };

                          await model.singUp(
                              userDataParam: userData,
                              pass: _passController.text,
                              onSuccess: () async {
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                                  content: Text('Usuário criado com sucesso.'),
                                  backgroundColor: Theme.of(context).primaryColor,
                                  duration: Duration(seconds: 2),
                                ));

                                await Future.delayed(Duration(seconds: 2), () {
                                  Navigator.of(context).pop();
                                });
                              },
                              onError: () {
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                                  content: Text('Erro ao criar usuário.'),
                                  backgroundColor: Colors.redAccent,
                                  duration: Duration(seconds: 2),
                                ));
                              }
                          );
                        }
                      },
                    ),
                  )
                ],
              ),
            );
          }
      ),
    );
  }
}