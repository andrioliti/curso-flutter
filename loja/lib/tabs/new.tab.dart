import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:transparent_image/transparent_image.dart';

class NewsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    _buildBodyBack() => Container(decoration: BoxDecoration(
      gradient: LinearGradient(
        colors: [
          Color.fromARGB(255, 211, 118, 130),
          Color.fromARGB(255, 253, 181, 168)
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      )
    ),);

    return Stack(
      children: <Widget>[
        _buildBodyBack(),
        CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              floating: true,
              snap: true,
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              flexibleSpace: FlexibleSpaceBar(
                title: const Text('Novidades'),
                centerTitle: true,
              ),
            ),
            FutureBuilder<QuerySnapshot>(
              future: Firestore.instance.collection('home').orderBy('order').getDocuments(),
              builder: (context, snp) {
                if (!snp.hasData) {
                  return SliverToBoxAdapter(
                    child: Center(
                      child: Container(
                        height: 300,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(Colors.white),
                        ),
                      ),
                    ),
                  );
                }

                return SliverStaggeredGrid.count(
                  crossAxisCount: 2,
                  mainAxisSpacing: 0,
                  crossAxisSpacing: 0,
                  staggeredTiles: snp.data.documents.map((it) => StaggeredTile.count(it.data['x'], it.data['y'])).toList(),
                  children: snp.data.documents.map((it) => FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: it.data['image'],
                    fit: BoxFit.cover,
                  )).toList(),
                );
              }
            )
          ],
        ),
      ],
    );
  }
}
