import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja/components/category.tile.dart';

class ProductTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      future: Firestore.instance.collection('produtos').getDocuments(),
      builder: (context, snp) {
        if (!snp.hasData) {
          return Container(
            child: Center(
              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Theme.of(context).primaryColor),),
            ),
          );
        }

        return ListView(
          children: ListTile.divideTiles(
            tiles: snp.data.documents.map((it) => CategoryTile(it)).toList(),
            color: Colors.grey[800]
          ).toList(),
        );
      },
    );
  }
}
