import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:youtube/models/video.dart';

const API_KEY = 'AIzaSyBIZNlex5Oq26IdPpXVywoma8SpfWrYQrI';


class Api {

  String _search;
  String _nextPage;

  search(String search) async {
    _search = search;

    http.Response res = await http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&q=$search&type=video&key=$API_KEY&maxResults=10");

    return decode(res);
  }

  decode(http.Response response) {
    if (response.statusCode == 200) {
      var decoded = json.decode(response.body);

      _nextPage = decoded['nextPageToken'];

      return decoded['items']
          .map<Video>((it) => Video.fromJson(it))
          .toList();
    } else {
      throw Exception('Faild to load videos');
    }
  }

  static getSugestions(String search) async {
    http.Response res = await http.get('http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q=$search&format=5&alt=json');

    if (res.statusCode != 200) {
      throw Exception('Error in loading sugestion');
    }

    return json.decode(res.body)[1].map((it) => it[0]).toList();
  }

  nextPage() async {
    http.Response res = await http.get('https://www.googleapis.com/youtube/v3/search?part=snippet&q=$_search&type=video&key=$API_KEY&maxResults=10&pageToken=$_nextPage');

    return decode(res);
  }
}