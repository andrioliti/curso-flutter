import 'dart:async';
import 'dart:convert';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:youtube/models/video.dart';

class FavoriteBloc extends BlocBase {

  final FAV_KEY = 'FAV_KEY';
  final _favoritesStream = BehaviorSubject<Map<String, Video>>();

  Map<String, Video> _favorites = { };

  get output => _favoritesStream.stream;

  FavoriteBloc() {
    SharedPreferences.getInstance().then((sharedPreferences) {
      if (sharedPreferences.containsKey(FAV_KEY)) {
        String _data = sharedPreferences.getString(FAV_KEY);
        _favorites = json.decode(_data).map((k,v) {
          return MapEntry(k, Video.fromJson(v));
        }).cast<String, Video>();

        _favoritesStream.sink.add(_favorites);
      }
    });
  }

  toggleFavoriteVideo(Video video) {
    if (_favorites.containsKey(video.id)) _favorites.remove(video.id);
    else _favorites[video.id] = video;

    _saveFavs();
    _favoritesStream.sink.add(_favorites);
  }

  _saveFavs() {
    SharedPreferences.getInstance().then((sharedPreferences) async {
      await sharedPreferences.setString(FAV_KEY, json.encode(_favorites));
    });
  }

  @override
  void dispose() {
    _favoritesStream.close();
    super.dispose();
  }
}