import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:youtube/api.dart';
import 'package:youtube/models/video.dart';

class VideoBloc extends BlocBase {
  List<Video> _videos;

  final _outStream = StreamController<List<Video>>.broadcast();
  final _inStream = StreamController<String>.broadcast();
  final _api = Api();

  Sink get input => _inStream.sink;
  Stream get output => _outStream.stream;

  VideoBloc() {
    _inStream.stream.listen((search) async {
      if (search != null) {
        _outStream.sink.add([]);
        _videos = await _api.search(search);
      } else {
        _videos += await _api.nextPage();
      }

      _outStream.sink.add(_videos);
    });
  }

  @override
  void dispose() {
    _inStream.close();
    _outStream.close();
    super.dispose();
  }
}