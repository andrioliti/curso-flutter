import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:youtube/api.dart';
import 'package:youtube/blocs/favorite_bloc.dart';
import 'package:youtube/models/video.dart';

class VideoTile extends StatelessWidget {

  Video _video;

  VideoTile(this._video);

  @override
  Widget build(BuildContext context) {

    final _favoriteBloc = BlocProvider.getBloc<FavoriteBloc>();

    return GestureDetector(
      onTap: () {
        FlutterYoutube.playYoutubeVideoById(apiKey: API_KEY, videoId: _video.id);
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          children: <Widget>[
            AspectRatio(
              aspectRatio: 16/9,
              child: Image.network(_video.thumb),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    _video.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18  ,
                    ),
                    maxLines: 2,
                  ),
                ),
                Expanded(
                  child: Text(
                    _video.channel,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15
                    ),
                    maxLines: 2,
                  ),
                ),
                StreamBuilder<Map<String, Video>>(
                  stream: _favoriteBloc.output,
                  initialData: { },
                  builder: (context, snapshot) {
                    if (snapshot.data == null) {
                      return CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Colors.white
                        ),);
                    } else {
                      return IconButton(
                        icon: snapshot.data.containsKey(_video.id) ?
                          Icon(Icons.star, color: Colors.white,) :
                        Icon(Icons.star_border, color: Colors.white,),
                        onPressed: () {
                          _favoriteBloc.toggleFavoriteVideo(_video);
                        },
                      );
                    }
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
