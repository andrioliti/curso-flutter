import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:youtube/blocs/favorite_bloc.dart';
import 'package:youtube/config/localization_custom.dart';
import 'package:youtube/screens/home.dart';

import 'blocs/video_bloc.dart';

void main() => runApp(YoutubeApp());

class YoutubeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => VideoBloc()),
        Bloc((i) => FavoriteBloc()),
      ],
      child: MaterialApp(
        localizationsDelegates: [
          CustomLocalizationDelegate(),
        ],
        debugShowCheckedModeBanner: false,
        home: Home(),
      ),
    );
  }
}
