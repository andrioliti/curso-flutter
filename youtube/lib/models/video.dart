class Video {
  String id = '';
  String name = '';
  String thumb = '';
  String channel = '';

  Video({this.id, this.name, this.channel, this.thumb});

  factory Video.fromJson(Map<String, dynamic> json) {
    if (json.containsKey('id')) {
      return Video(
          id: json['id']['videoId'],
          name: json['snippet']['title'],
          thumb: json['snippet']['thumbnails']['high']['url'],
          channel: json['snippet']['channelTitle'],
      );
    } else {
      return Video(
        id: json['videoId'],
        name: json['name'],
        thumb: json['thumb'],
        channel: json['channel'],
      );
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'videoId': id,
      'name': name,
      'thumb': thumb,
      'channel': channel,
    };
  }

}