import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:youtube/api.dart';
import 'package:youtube/blocs/favorite_bloc.dart';
import 'package:youtube/models/video.dart';

class FavoriteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final _favoriteBloc = BlocProvider.getBloc<FavoriteBloc>();

    return Scaffold(
      backgroundColor: Colors.black54,
      appBar: AppBar(
        backgroundColor: Colors.black54,
        centerTitle: true,
        title: Text('Favoritos'),
      ),
      body: StreamBuilder<Map<String, Video>>(
        stream: _favoriteBloc.output,
        initialData: { },
        builder: (context, snapshot) {
          return ListView(
            children: snapshot.data.values.map((it) {
              return InkWell(
                onTap: () {
                  FlutterYoutube.playYoutubeVideoById(apiKey: API_KEY, videoId: it.id);
                },
                onLongPress: () {
                  _favoriteBloc.toggleFavoriteVideo(it);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 100,
                      height: 50,
                      child: Image.network(it.thumb),
                    ),
                    Expanded(
                      child: Text(it.name, style: TextStyle(color: Colors.white30), maxLines: 2,),

                    )
                  ],
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
