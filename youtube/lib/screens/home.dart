import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:youtube/blocs/favorite_bloc.dart';
import 'package:youtube/blocs/video_bloc.dart';
import 'package:youtube/components/video_tile.dart';
import 'package:youtube/delegates/data_delegate.dart';
import 'package:youtube/models/video.dart';
import 'package:youtube/screens/favorite.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final _videoBloc = BlocProvider.getBloc<VideoBloc>();
    final _favBloc = BlocProvider.getBloc<FavoriteBloc>();

    return Scaffold(
      backgroundColor: Colors.black38,
      appBar: AppBar(
        title: Container(
          height: 100,
          child: Image.asset('images/youtube_logo.png'),
        ),
        backgroundColor: Colors.black38,
        actions: <Widget>[
          Align(
            alignment: Alignment.center,
            child: StreamBuilder<Map<String, Video>>(
                initialData: { },
                stream: _favBloc.output,
              builder: (context, snapshot) {
                if (snapshot.data != null) {
                  return Text('${snapshot.data.length}');
                }

                return Text('0');
              }
            ),
          ),
          IconButton(
            icon: Icon(Icons.star),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => FavoriteScreen()));
            },),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              var query = await showSearch(context: context, delegate: DataDelegate());
              _videoBloc.input.add(query);
            },),
        ],
      ),
      body: StreamBuilder<List<Video>>(
        stream: _videoBloc.output,
        initialData: [],
        builder: (context, snapshot) {
          return Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            child: ListView.builder(
              itemCount: snapshot.data.length+1,
              itemBuilder: (context, index) {
                if (snapshot.data.isEmpty) {
                  return Container();
                } else if(index >= snapshot.data.length) {
                  _videoBloc.input.add(null);

                  return Container(
                    height: 80,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                    ),
                    alignment: Alignment.center,
                  );
                } else {
                  return VideoTile(snapshot.data[index]);
                }
              },
            ),
          );
        }
      ),
    );
  }
}